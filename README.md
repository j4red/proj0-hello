# Proj0-Hello
-------------

**Author:** Jared Knofczynski  
**Contact:** jknofczy@uoregon.edu  
**Description:** My implementation of Project 0 for CIS 322. Prints the message
'Hello world' as specified by the file `credentials.ini`.  

## Instructions:
---------------

- Ensure you have placed `credentials.ini` in the `hello` subdirectory.

- Use the command ``make run`` in the `/proj0-hello/` directory to execute the program.
